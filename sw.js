const VERSION = '0.0.10';
const CACHE_NAME = 'youlang-cache-v1';
const urlsToCache = [
  '/home',
];

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME)
    .then((cache) => cache.addAll(urlsToCache))
  );
});

self.addEventListener('activate', event => {
  console.log("activated");
})

self.addEventListener('fetch', event => {
  if(event.request.method==="POST") return false;
  if(event.request.url.includes('reissue')) return false;
  else {
    event.respondWith(
      caches.open(CACHE_NAME).then(cache => {
        return cache.match(event.request).then(response => {
          return response || fetch(event.request).then(response => {
            cache.put(event.request, response.clone());
            return response;
          });
        });
      })
    );
  }
});
