"use strict";
class Word {
	#_id;
	#word;
	#explaination;
	#group;
	#unit;
	#topic;
	#createdAt;
	#updatedAt;
	#createdBy;
	#flipped;
	constructor(object) {
		this.#_id = object._id;
    this.#word = object.word;
    this.#explaination = object.explaination;
    this.#group = object.group;
    this.#unit = object.unit;
    this.#topic = object.topic;
    this.#createdAt = object.createdAt;
    this.#updatedAt = object.updatedAt;
    this.#createdBy = object.createdBy;
		this.#flipped = false;
	}
	get word() {return this.#word};
	get id() {return this.#_id};
	get explaination() { return this.#explaination};
	get createdAt() { return this.#createdAt};
	get flipped() {return this.#flipped}
	flip = () => this.#flipped = !this.#flipped;
	toJSON = () => JSON.parse(JSON.stringify({
		id: this.#_id,
		word: this.#word,
		explaination: this.#explaination,
		group: this.#group,
		unit: this.#unit,
		topic: this.#topic
	}));
	toNode = () => {
		let div = generateElement('div', {class: "word"});
		div.innerHTML = `<span>${this.#word}</span><span>${this.#explaination}</span>`;
		let touchstart = (e) => {
			if(e.target.tagName === 'SPAN') return;
			div.classList.add('pressed');
		}
		let touchend = () => div.classList.remove('pressed');
		let displayWordPopup = (e) => {
			if(e.target.tagName === 'SPAN') return;
			if(e) e.preventDefault();
			document.querySelectorAll('.wordPopup').forEach(x=>x.remove());
			window.navigator.vibrate(10);
			let popup = generateElement('div', {class: "wordPopup"});
			let removeBtn = generateElement('button');
			removeBtn.innerHTML = `<i class='material-icons'>delete</i>`;
			removeBtn.onclick = () => {
				this.remove();
			}
			let editBtn = generateElement('button');
			editBtn.innerHTML = `<i class='material-icons'>edit</i>`;
			editBtn.onclick = () => {
				console.log('edit')
			}
			let infoBtn = generateElement('button');
			infoBtn.innerHTML = `<i class='material-icons'>info</i>`;
			infoBtn.onclick = () => {
				let html = `
	      <div class="modal-content">
	        <div class="modal-header">
	          <span id="closeModal">&times;</span>
	          <h5>${this.#word}</h5>
	          <h5>${this.#explaination}</h5>
	        </div>
	        <div class="modal-body">`;
	      console.log(this.#topic)
				if(this.#topic) html+= `<div>Unit: ${this.#unit.name}</div><div>Topic: ${this.#topic.name}</div>`;
				// unit and topic could redirect to book
	      html+= `
	          <div>CreatedBy: ${this.#createdBy}</div>
	          <div>UpdatedAt: ${this.#updatedAt}</div>
	        </div>
	      </div>`;
				Scene.modal(html);
			}
			document.querySelector('body').addEventListener('click', (e) => {
				if(!e.target.closest('.wordPopup')) popup.remove();
			})
			popup.appendChild(infoBtn);
			popup.appendChild(editBtn);
			popup.appendChild(removeBtn);
			div.appendChild(popup);
		};
		div.addEventListener("dblclick", (e) => {
			if(e.target.closest('.wordPopup')) return;
			div.style.backgroundColor = 'yellow';
		})
		div.addEventListener("touchstart", touchstart);
	  div.addEventListener("touchend", touchend);
	  div.addEventListener("contextmenu", displayWordPopup);

		return div;
	}
	save = () => {
		let body = {
      word: this.#word,
      explaination: this.#explaination,
      groupId: this.#group
    }
    if(this.#unit) body.unit = this.#unit;
    if(this.#topic) body.topic = this.#topic;
    fetch("youlang/word/", {
      method: "POST",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify(body)
    })
    .then(async(res) => {
      console.log(res)
      if(!res.ok) {
        res.text().then(text => toast(text, 4000, {backgroundColor: '#ff5050'}));
        if(res.status === 403) {
          await reissueToken();
          this.save();
        }
      }
      else {
        toast('Added word', 4000, {backgroundColor: '#9ccc65'});
        let word = await res.json();
        events.emit('newWord', word);
      }
    });
  }
  remove = async() => {
  	await fetch(`youlang/word/${this.#_id}/`, {
      method: "DELETE",
      headers: setHeaders(),
      referrer: "no-referrer"
    })
    .then(async(res) => {
      if(!res.ok) {
        res.text().then(text => toast(text, 4000, {backgroundColor: '#ff5050'}));
        if(res.status === 403) {
          await reissueToken();
          this.remove();
        }
      }
      else {
        toast('Removed word', 4000, {backgroundColor: '#9ccc65'});
        events.emit('wordRemoved', {_id: this.#_id, group: this.#group})
      }
    });
  }
}
