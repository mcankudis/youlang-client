class HomeScreen {
  #username;
  constructor(username) {
    this.#username = username;
  }
  generateTemplate = () => {
    let template = `
      <h2 style="text-align: center">Hello ${this.#username}</h2>
      <h3>Welcome home!</h3>`
    let node = htmlToFragment(template);
    return node;
  }
}