"use strict";
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
function findYOffset(obj) {
  var curtop = 0;
  if (obj.offsetParent) {
      do {
          curtop += obj.offsetTop;
      } while (obj = obj.offsetParent);
  return curtop;
  }
}
const generateElement = (tag, options, children) => {
  if(!tag) return;
  let node = document.createElement(tag);
  if(!options) return node;
  if(options.type) node.type = options.type;
  if(options.id) node.id = options.id;
  if(options.class) node.className = options.class;
  if(options.placeholder) node.placeholder = options.placeholder;
  if(typeof options.value!=='undefined') node.value = options.value;
  if(options.href) node.href = options.href;
  if(options.innerHTML) node.innerHTML = options.innerHTML;
  if(options.style) node.style = options.style;
  if(options.for) node.for = options.for;
  if(!children) return node;
  for (let i = 0; i < children.length; i++) {
    node.appendChild(children[i])
  }
  return node;
}
const htmlToFragment = (html) => {
  let template = document.createElement('template');
  template.innerHTML = html;
  return template.content;
}
const logOut = (auto) => {
  console.log('logging out')
  if(!auto) toast("Logging out", 4000, {backgroundColor: '#9ccc65'});
  localStorage.clear();
  window.localStorage.clear();
  window.location.href="/login";
}
const reissueToken = async() => {
  let res = await fetch('/user/token/reissue', {
    method: "GET",
    headers: {
        "Content-Type": "application/json",
        "x-auth": app.token,
        "x-reissue": app.reissue_token
    },
    referrer: "no-referrer"
  })
  console.log('reissue data', res)
  if(!res.ok) {
    let text = await res.text();
    console.log(text);
    toast(text, 4000, {backgroundColor: '#ff5050'});
    logOut();
  }
  let body = await res.json();
  localStorage.setItem('token', body.token);
  localStorage.setItem('reissueToken', body.reissueToken);
  app.token = body.token;
  app.reissue_token = body.reissueTtoken;
  return 1;
}
const setHeaders = () => {
  return {
    "Content-Type": "application/json",
    "x-auth": localStorage.getItem("token")
  }
}
const toast = (msg, time, style) => {
  if(!time) time = 4000;
  let backgroundColor, color, fontSize;
  if(style) {
    backgroundColor = style.backgroundColor,
    color = style.color, fontSize = style.fontSize;
  }
  const toast = document.createElement('div');
  toast.style.backgroundColor = backgroundColor || '#424242';
  toast.style.display = 'inline-block';
  toast.style.position = 'fixed';
	toast.style.width = `${window.innerWidth-30}px`;
  toast.style.left = '4px';
  toast.style.top = '10px';
  toast.style.padding = '10px';
  toast.style.color = color || 'white';
  toast.style.fontSize = fontSize || '1.5rem';
  toast.style.borderRadius = '4px';
	toast.style.border = 'solid 1px #bbbbbb';
  toast.style.zIndex = '999';
  toast.innerHTML = msg;
  const body = document.getElementsByTagName('body');
  body[0].appendChild(toast);
  setTimeout(()=>{
    toast.remove();
  }, time)
}
