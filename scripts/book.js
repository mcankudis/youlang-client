"use strict"
class Unit {
  #_id;
  #name;
  #description;
  #group;
  #number;
  #createdAt;
  #updatedAt;
  #updatedBy;
  #topics;
  constructor(object) {
    this.#_id = object._id;
    this.#name = object.name;
    this.#description = object.description;
    this.#group = object.group;
    this.#number = object.number;
    this.#createdAt = object.createdAt;
    this.#updatedAt = object.updatedAt;
    this.#updatedBy = object.updatedBy;
    this.#topics = [];
    if(object.topics) this.#topics = object.topics.map(t => new Topic(t));
    events.register('newTopic', (t) => {
      if(t.unit === this.#_id) this.#topics.push(new Topic(t));
    }, 0);
  }
  get id() {
    return this.#_id;
  }
  get name() {
    return this.#name;
  }
  get number() {
    return this.#number;
  }
  get topics() {
    return this.#topics;
  }
  get group() {
    return this.#group;
  }
  toHTML = () => {
    let html = `<li>${this.#number}. ${this.#name}<ul>`
    this.#topics.forEach(t => html+= t.toHTML());
    html+=`</ul></li>`
    return html;
  }
  fetchTopics = async() => {
    await fetch(`youlang/unit/${this.#_id}/topics/`, {
      method: "GET",
      headers: setHeaders(),
      referrer: "no-referrer"
    })
    .then(async(res) => {
      if(!res.ok) {
        res.text().then(text => toast(text, 4000, {backgroundColor: '#ff5050'}));
        if(res.status === 403) {
          await reissueToken();
          this.fetch();
        }
      }
      else {
        toast('Fetched topics', 4000, {backgroundColor: '#9ccc65'});
        let t = await res.json();
        this.#topics = t.map(x => new Topic(x));
        this.sortTopics();
      }
    });
  }
  sortTopics = () => {
    this.#topics.sort((a,b)=> a.number-b.number);
  }
  save = () => {
    fetch("youlang/unit/", {
      method: "POST",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify({
          name: this.#name,
          description: this.#description,
          number: this.#number,
          groupId: this.#group
      })
    })
    .then(async(res) => {
      console.log(res)
      if(!res.ok) {
        res.text().then(text => toast(text, 4000, {backgroundColor: '#ff5050'}));
        if(res.status === 403) {
          await reissueToken();
          this.save();
        }
      }
      else {
        toast('Added unit', 4000, {backgroundColor: '#9ccc65'});
        let unit = new Unit(await res.json());
        events.emit('newUnit', unit);
      }
    });
  }
}
class Topic {
  #_id;
  #name;
  #description;
  #unit;
  #group;
  #number;
  #createdAt;
  #updatedAt;
  #updatedBy;
  constructor(object) {
    this.#_id = object._id;
    this.#name = object.name;
    this.#description = object.description;
    this.#unit = object.unit;
    this.#group = object.group;
    this.#number = object.number;
    this.#createdAt = object.createdAt;
    this.#updatedAt = object.updatedAt;
    this.#updatedBy = object.updatedBy;
  }
  get id() {
    return this.#_id;
  }
  get name() {
    return this.#name;
  }
  get number() {
    return this.#number;
  }
  get unit() {
    return this.#unit;
  }
  toHTML = () => `<li>${this.#name}</li>`
  save = () => {
    fetch("youlang/topic/", {
      method: "POST",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify({
          name: this.#name,
          description: this.#description,
          number: this.#number,
          unitId: this.#unit,
          groupId: this.#group
      })
    })
    .then(async(res) => {
      console.log(res)
      if(!res.ok) {
        res.text().then(text => toast(text, 4000, {backgroundColor: '#ff5050'}));
        if(res.status === 403) {
          await reissueToken();
          this.save();
        }
      }
      else {
        toast('Added topic', 4000, {backgroundColor: '#9ccc65'});
        let topic = await res.json();
        topic = new Topic(topic);
        events.emit('newTopic', topic);
      }
    });
  }
}
