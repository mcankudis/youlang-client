class Scene {
  static displayNode(node) {
    this.clear();
    document.querySelector('main').appendChild(node);
  }
  static clear() {
    // document.removeEventListener('wheel');
    document.querySelector('body').style.overflow = 'scroll';
    document.querySelector('main').innerHTML = "";
  }
  static updateGroupsSelect(groups) {
    let options = document.createDocumentFragment();
    for(let i = 0; i<groups.length; i++) {
      let option = generateElement('option', {innerHTML: groups[i].name, value: i});
      options.appendChild(option);
    }
    let groupsSelectNode = document.querySelector('#groupsSelect');
    groupsSelectNode.innerHTML = "";
    groupsSelectNode.onchange = (e) => {
      let i = groupsSelectNode.options[groupsSelectNode.selectedIndex].value;
      events.emit('groupSelected', groups[i]);
    }
    groupsSelectNode.appendChild(options);
  }
  static updateState(state) {
    let a = document.querySelector('.activeNav');
    if(a) a.classList.remove('activeNav');
    document.querySelector(`#${state} a`).classList.add('activeNav');
    window.location.hash = `#${state}`;
  }
  static modal(html) {
    let modal = generateElement('div', {id: "myModal", class: "modal"})
    modal.innerHTML = html;
    window.onclick = e => (e.target == modal) ? modal.remove() : false;
    document.querySelector('main').appendChild(modal);
    let closeBtn = document.querySelector("#closeModal");
    closeBtn.onclick = () => modal.remove();
  }
}
