const changeUsername = (username, password) => {
  const formData = {newUsername: username, password};
  fetch("/user/username/change", {
    method: "POST",
    headers: setHeaders(),
    referrer: "no-referrer",
    body: JSON.stringify(formData)
  })
  .then(async(res) => {
    if(!res.ok) {
      if(res.status===403) {
        await reissueToken();
        changeUsername(username, password);
      }
      else res.text().then(text=> toast(text, 4000, {backgroundColor: '#ff5050'}));
    }
    else {
      let body = await res.json();
      toast('Username changed', 4000, {backgroundColor: '#9ccc65'});
      app.user.username = body.username;
      // document.getElementById('usernameDisplay').innerHTML = body.username;
      localStorage.setItem('username', body.username);
      localStorage.setItem('token', body.token);
    }
  });
}

const changePassword = (newPassword, password) => {
  const formData = {newPassword, password}
  fetch("/user/password/change", {
    method: "POST",
    headers: setHeaders(),
    referrer: "no-referrer",
    body: JSON.stringify(formData)
  })
  .then(async(res) => {
    if(!res.ok) {
      if(res.status===403) {
        await reissueToken();
        changeUsername(newPassword, password);
      }
      else res.text().then(text=> toast(text, 4000, {backgroundColor: 'red'}));
    }
    else {
      toast('Password changed', 4000, {backgroundColor: '#9ccc65'});
    }
  });
}
class Settings {
	#username;
	constructor(username) {
	  this.#username = username;
	}
  generateTemplate = () => {
    let template = `
      <h2 style="text-align: center">Hello ${this.#username}</h2>
      <h3>Welcome to your account management panel!</h3>
      <form id="onLoadGroup">
        <h4>You can pick the group that should be open when you reload the app</h4>
        <div class="row">
          <select id="onLoadGroupSelect" class="six columns">
            <option disabled selected">Choose a group</option>`;
            // todo: add favourite group to user model and make an API endpoint
template += `</select>
          <button type="submit">Confirm</button>
        </div>
      </form>
      <form id="usernameForm">
        <h4>You can change your username:</h4>
        <div class="row">
          <div class="six columns">
            <label for="newUsername">New username</label>
            <input type="text" id="newUsername" class="u-full-width"></input>
          </div>
          <div class="six columns">
            <label for="password">Password</label>
            <input type="password" id="password" class="u-full-width"></input>
          </div>
        </div>
        <button type="submit">Confirm</button>
      </form>
      <form id="passwordForm">
        <h4>Or you can change your password:</h4>
        <div class="row">
          <div  class="six columns">
            <label for="newPassword">New password</label>
            <input type="password" id="newPassword" class="u-full-width"></input>
          </div>
          <div class="six columns">
            <label for="password2">Old password</label>
            <input type="password" id="password2" class="u-full-width"></input>
          </div>
        </div>
        <button type="submit">Confirm</button>
      </form>
      <button class="red" onclick="logOut()">Log out</button>
    `
    let node = htmlToFragment(template);
    return node;
  }
  attachEvents = () => {
    // todo onLoadGroup.submit event
    const usernameForm = document.getElementById('usernameForm');
    usernameForm.addEventListener('submit', (e) => {
      e.preventDefault();
      const username = document.getElementById('newUsername').value;
      const password = document.getElementById('password').value;
      changeUsername(username, password);
    })
    const passwordForm = document.getElementById('passwordForm');
    passwordForm.addEventListener('submit', (e) => {
      e.preventDefault();
      const newPassword = document.getElementById('newPassword').value;
      const password = document.getElementById('password2').value;
      changePassword(newPassword, password);
    })
  }
}
