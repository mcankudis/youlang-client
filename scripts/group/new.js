class newGroup {
  #name;
  #description;
  constructor(object) {
    this.#name = object.name;
    this.#description = object.description;
  }
  save = (callback) => {
    fetch("youlang/group/", {
      method: "POST",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify({
          name: this.#name,
          description: this.#description
      })
    })
    .then(async(res) => {
      if(!res.ok) {
        if(res.status === 403) {
          await reissueToken();
          this.save();
        }
        else if(res.status === 400) res.text().then(text => toast(`Failded to add group: ${text}`, 3000, {backgroundColor: '#ff5050'}));
        else toast("Failed to add group: unknown error", 3000, {backgroundColor: '#ff5050'});
      }
      else {
        toast('Added group', 4000, {backgroundColor: '#9ccc65'});
        let group = new Group(await res.json());
        events.emit('newGroup', group);
      }
    });
  }
}
