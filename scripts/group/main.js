"use strict";
class Group {
  #_id;
  #name;
  #description;
  #users;
  #admins;
  #createdAt;
  #updatedAt;
  #updatedBy;
  #units;
  #words;
  #wordsToDisplay;
  #wordsSortedBy;
  #wordsDisplayForm;
  constructor(object) {
    this.#_id = object._id;
    this.#name = object.name;
    this.#description = object.description;
    this.#users = object.users;
    this.#admins = object.admins;
    this.#createdAt = object.createdAt;
    this.#updatedAt = object.updatedAt;
    this.#updatedBy = object.updatedBy;
    this.#units = [];
    if(object.units) this.#units = object.units.map(u => new Unit(u));
    this.#words = [];
    if(object.words) this.#words = object.words.map(w => new Word(w));
    this.#wordsSortedBy = 'create_date';
    events.register('newUnit', (u) => {
      if(u.group === this.#_id) this.#units.push(new Unit(u));
    }, 0);
    events.register('newWord', (w) => {
      if(w.group === this.#_id) this.#words.push(new Word(w));
    }, 0);
    events.register('wordRemoved', (w) => {
      if(w.group === this.#_id) this.#words = this.#words.filter(x => (x.id!==w._id));
    }, 0);
    this.#wordsToDisplay = this.#words;
    let startX, startY, startTime;
    document.addEventListener('touchstart', e => {
      if(this.#wordsDisplayForm!=='cards' || app.state!=='dictionary') return;
      let touchParams = e.changedTouches[0];
      startX = touchParams.screenX;
      startY = touchParams.screenY;
      startTime = new Date().getTime();
    })
    document.addEventListener('touchmove', e => {
      if(this.#wordsDisplayForm!=='cards' || app.state!=='dictionary') return;
      e.preventDefault();
    }, {passive: false})
    document.addEventListener('touchend', e => {
      if(this.#wordsDisplayForm!=='cards' || app.state!=='dictionary') return;
      let toleranceX = 100;
      let minDownY = -50;
      let minUpY = 50;
      let maxTime = 300;
      let touchParams = e.changedTouches[0];
      let lengthX = touchParams.screenX - startX;
      let lengthY = touchParams.screenY - startY;
      if(new Date().getTime() - startTime > maxTime) return;
      if(lengthY>=minUpY && lengthX<toleranceX) events.emit('scrollToCard', 0); // swipe up
      if(lengthY<=minDownY && lengthX<toleranceX) events.emit('scrollToCard', 1); //swipe down
    })
    document.addEventListener('wheel', debounce((e) => {
			if(this.#wordsDisplayForm!=='cards' || app.state!=='dictionary' || window.innerWidth>749) return;
			if(e.deltaY>0) events.emit('scrollToCard', 1);
			else if(e.deltaY<0) events.emit('scrollToCard', 0);
		}, 50));
		document.addEventListener('keydown', (e) => {
			if(this.#wordsDisplayForm!=='cards' || app.state!=='dictionary') return;
			if(e.code==='ArrowDown')	events.emit('scrollToCard', 1);
			else if(e.code==='ArrowUp') events.emit('scrollToCard', 0);
		});
  }
  get name() {
    return this.#name;
  }
  get id() {
    return this.#_id;
  }
  get users() {
    return this.#users;
  }
  get admins() {
    return this.#admins;
  }
  get units() {
    return this.#units;
  }
  get words() {
    return this.#words;
  }
  get wordsSortedBy() {
    return this.#wordsSortedBy;
  }
  get wordsToDisplay() {
    return this.#wordsToDisplay;
  }
  get wordsDisplayForm() {
    return this.#wordsDisplayForm;
  }
  refreshWords = (form) => {
    if(form) this.#wordsDisplayForm = form;
    if(this.#wordsDisplayForm === 'cards') {
      GroupDisplayer.updateWords(GroupDisplayer.wordsToCardsNode(this.#wordsToDisplay));
      if(window.innerWidth<750) document.querySelector('body').style.overflow = 'hidden';
      let i = 0;
      events.register('scrollToCard', (direction) => {
        if(!direction && i>0) {
          i--;
          window.scrollTo({ top: findYOffset(document.getElementById(`card${i}`))-window.innerHeight*0.20, behavior: 'smooth'});
        }
        else if(direction && i<this.#wordsToDisplay.length-1){
          i++;
          window.scrollTo({ top: findYOffset(document.getElementById(`card${i}`))-window.innerHeight*0.20, behavior: 'smooth'});
        }
      }, -1)
    }
    else GroupDisplayer.updateWords(GroupDisplayer.wordsToListNode(this.#wordsToDisplay, this));
  }
  openWordList = () => {
    this.#wordsDisplayForm = 'list';
    let template = GroupDisplayer.wordsToListNode(this.#words);
    GroupDisplayer.updateWords(template);
  }
  openIndexCards = () => {
    this.#wordsDisplayForm = 'cards';
    let template = GroupDisplayer.wordsToCardsNode(this.#words);
    GroupDisplayer.updateWords(template);
  }
  downloadWordsTxt = () => {
    let filename = `words_backup_group_${this.#name}`;
    let text = [];
    this.#words.forEach(w => {
      console.log(w.toJSON());
      text.push(w.toJSON());
    })
    text = JSON.stringify(text);
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    let x = document.createDocumentFragment();
    x.appendChild(element);
    element.click();
  }
  searchWords = (phrase) => {
    this.#wordsToDisplay = this.#words.filter(x=> x.word.includes(phrase) || x.explaination.includes(phrase));
    let sortBy = this.#wordsSortedBy;
    this.#wordsSortedBy = '';
    this.sortWords(sortBy);
    return this.#wordsToDisplay;
  }
  sortUnits = () => this.#units.sort((a,b)=> a.number-b.number);
  sortWords = (type) => {
    switch (type) {
      case 'word':
        if(this.#wordsSortedBy !== 'word') {
          this.#wordsToDisplay.sort((a,b) => {return (a.word > b.word) ? 1 : ((b.word > a.word) ? -1 : 0);});
          this.#wordsSortedBy = 'word';
        }
        else {
          this.#wordsToDisplay.sort((a,b) => {return (a.word > b.word) ? -1 : ((b.word > a.word) ? 1 : 0);} );
          this.#wordsSortedBy = 'word_reverse';
        }
        break;
      case 'explaination':
        if(this.#wordsSortedBy !== 'explaination') {
          this.#wordsToDisplay.sort((a,b) => {return (a.explaination > b.explaination) ? 1 : ((b.explaination > a.explaination) ? -1 : 0);} );
          this.#wordsSortedBy = 'explaination';
        }
        else {
          this.#wordsToDisplay.sort((a,b) => {return (a.explaination > b.explaination) ? -1 : ((b.explaination > a.explaination) ? 1 : 0);} );
          this.#wordsSortedBy = 'explaination_reverse';
        }
        break;
      case 'create_date':
        if(this.#wordsSortedBy !== 'create_date') {
          this.#wordsToDisplay.sort((a,b) => {return (a.createdAt > b.createdAt) ? 1 : ((b.createdAt > a.createdAt) ? -1 : 0);} );
          this.#wordsSortedBy = 'create_date';
        }
        else {
          this.#wordsToDisplay.sort((a,b) => {return (a.createdAt > b.createdAt) ? -1 : ((b.createdAt > a.createdAt) ? 1 : 0);} );
          this.#wordsSortedBy = 'create_date_reverse';
        }
        break;
    }
  }
  addUser = (username) => {
    fetch("youlang/group/user", {
      method: "POST",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify({
        group: this.#_id,
        user: username
      })
    })
    .then(async(res) => {
      console.log(res)
      if(!res.ok) {
        if(res.status === 403) {
          await reissueToken();
          this.addUser(username);
        }
        else if(res.status === 400) res.text().then(text => toast(text, 3000, {backgroundColor: '#ff5050'}));
        else toast("Failed to add user: unknown error", 3000, {backgroundColor: '#ff5050'});
      }
      else {
        toast('Added user', 4000, {backgroundColor: '#9ccc65'});
        this.#users.push(await res.json());
        GroupDisplayer.displayGroup(this);
      }
    });
  }
  removeUser = (userId) => {
    fetch("youlang/group/user", {
      method: "DELETE",
      headers: setHeaders(),
      referrer: "no-referrer",
      body: JSON.stringify({
        group: this.#_id,
        user: userId
      })
    })
    .then(async(res) => {
      console.log(res)
      if(!res.ok) {
        if(res.status === 403) {
          toast("Refreshing...", 1000);
          await reissueToken();
          this.removeUser(userId);
        }
      }
      else {
        if(userId === app.user.id) {
          toast(`Left the group ${this.#name}`, 4000, {backgroundColor: '#9ccc65'});
          events.emit('leftGroup', this.#_id);
        }
        else {
          toast(`Removed user from group ${this.#name}`, 4000, {backgroundColor: '#9ccc65'});
          console.log(await res.json());
          this.open();
        }
      }
    });
  }
}
