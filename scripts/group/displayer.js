class GroupDisplayer {
  static templateBook = (group) => {
    let template = document.createDocumentFragment();
    let bookForm = generateElement('div', {id: "bookForm", class: "container"});
    let unitForm = generateElement('form');
    unitForm.innerHTML = `
    <div class="row">
      <input type="text" id="newUnitName" class="six columns" placeholder="New units' name"/>
      <input type="text" id="newUnitDescription" class="six columns" placeholder="New units' description"/>
    </div>
    <div class="row">
      <input type="number" id="newUnitNumber" class="six columns" placeholder="New units' number"/>
    </div>
    <button type="submit">Add unit</button>`
    unitForm.onsubmit = (e) => {
      e.preventDefault();
      let u = new Unit({
        name: document.querySelector('#newUnitName').value,
        description: document.querySelector('#newUnitDescription').value,
        number: document.querySelector('#newUnitNumber').value,
        group: group.id
      });
      u.save();
    }
    let topicForm = generateElement('form');
    let tf = `
    <div class="row">
      <input type="text" id="newTopicName" class="six columns" placeholder="New topics' name"/>
      <input type="text" id="newTopicDescription" class="six columns" placeholder="New topics' description"/>
    </div>
    <div class="row">
      <select id="newTopicUnit" class="six columns">
        <option disabled selected value="">Topics' unit</option>`
          let list = group.units;
          for (let i = 0; i < list.length; i++)
            tf+=`<option value="${list[i].id}">${list[i].name}</option>`
    tf+=`</select>
      <input type="text" id="newTopicNumber" class="six columns" placeholder="New topics' number"/>
    </div>
    <button type="submit">Add topic</button>`
    topicForm.innerHTML = tf;
    topicForm.onsubmit = (e) => {
      e.preventDefault();
      let t = new Topic({
        name: document.querySelector('#newTopicName').value,
        description: document.querySelector('#newTopicDescription').value,
        number: document.querySelector('#newTopicNumber').value,
        unit: document.querySelector('#newTopicUnit').value,
        group: group.id
      });
      t.save();
    }
    let openFormBtn = generateElement('div', {innerHTML: "Add unit or topic", class: "button"});
    openFormBtn.onclick = () => bookForm.classList.add('unfolded');
    let closeFormBtn = generateElement('div', {innerHTML: "Close", class: "button u-pull-right red"});
    closeFormBtn.onclick = () => bookForm.classList.remove('unfolded');
    bookForm.appendChild(unitForm);
    bookForm.appendChild(topicForm);
    template.appendChild(bookForm);
    topicForm.appendChild(closeFormBtn);
    template.appendChild(openFormBtn);
    template.appendChild(htmlToFragment(`<h4>Units and topics of the "${group.name}" group</h4>`))
    if(group.units.length===0) {
      template.appendChild(htmlToFragment(`<div>It seems the book of this group is still empty. Add some units and topics to see them here!</div>`));
      Scene.displayNode(template);
      return;
    }
    let html =`<ul id="bookList">`
    group.units.forEach(u => html+=u.toHTML());
    html+=`</ul>`;
    template.appendChild(htmlToFragment(html));
    Scene.displayNode(template);
  }
  static templateWords = (group) => {
    let template = document.createDocumentFragment();
    // searchFormSlider
    let searchForm = generateElement('form', {class: 'rightSlider', id: 'searchWords'});
    searchForm.innerHTML = `
      <i class="material-icons md-18">search</i>
      <input type="text" id="searchedWord" placeholder="Search words"/>
      <button type="submit">Search</button>`;
    searchForm.onsubmit = (e) => {
      e.preventDefault();
      let phrase = document.querySelector('#searchedWord').value;
      let words = group.searchWords(phrase);
      group.refreshWords();
    }
    template.appendChild(searchForm);
    // eof searchFormSlider
    // sortSlider
    let sortSlider = generateElement('div', {class: 'rightSlider', id: 'sortWords', innerHTML: `<i class="material-icons md-18">sort</i>`});
    let sortByWord = generateElement('button', {innerHTML: `<i class="material-icons">sort_by_alpha</i>`});
    let sortByExplaination = generateElement('button', {innerHTML: `<i class="material-icons">translate</i>`});
    let sortByDate = generateElement('button', {innerHTML: `<i class="material-icons">date_range</i>`});
    if(group.wordsSortedBy === 'word') sortByWord.innerHTML += `<i class="material-icons">arrow_drop_down</i>`;
    if(group.wordsSortedBy === 'word_reverse') sortByWord.innerHTML += `<i class="material-icons">arrow_drop_up</i>`;
    if(group.wordsSortedBy === 'explaination') sortByExplaination.innerHTML += `<i class="material-icons">arrow_drop_down</i>`;
    if(group.wordsSortedBy === 'explaination_reverse') sortByExplaination.innerHTML += `<i class="material-icons">arrow_drop_up</i>`;
    if(group.wordsSortedBy === 'create_date') sortByDate.innerHTML += `<i class="material-icons">arrow_drop_down</i>`;
    if(group.wordsSortedBy === 'create_date_reverse') sortByDate.innerHTML += `<i class="material-icons">arrow_drop_up</i>`;
    sortByWord.onclick = () => {
      group.sortWords('word');
      group.refreshWords();
      sortByDate.innerHTML = `<i class="material-icons">date_range</i>`;
      sortByExplaination.innerHTML = `<i class="material-icons">translate</i>`;
      if(group.wordsSortedBy === 'word') sortByWord.innerHTML = `<i class="material-icons">sort_by_alpha</i><i class="material-icons">arrow_drop_down</i>`;
      else sortByWord.innerHTML = `<i class="material-icons">sort_by_alpha</i><i class="material-icons">arrow_drop_up</i>`;
    }
    sortByExplaination.onclick = () => {
      group.sortWords('explaination');
      group.refreshWords();
      sortByWord.innerHTML = `<i class="material-icons">sort_by_alpha</i>`;
      sortByDate.innerHTML = `<i class="material-icons">date_range</i>`;
      if(group.wordsSortedBy === 'explaination') sortByExplaination.innerHTML = `<i class="material-icons">translate</i><i class="material-icons">arrow_drop_down</i>`;
      else sortByExplaination.innerHTML = `<i class="material-icons">translate</i><i class="material-icons">arrow_drop_up</i>`;
    }
    sortByDate.onclick = () => {
      group.sortWords('create_date');
      group.refreshWords();
      sortByWord.innerHTML = `<i class="material-icons">sort_by_alpha</i>`;
      sortByExplaination.innerHTML = `<i class="material-icons">translate</i>`;
      if(group.wordsSortedBy === 'create_date') sortByDate.innerHTML = `<i class="material-icons">date_range</i><i class="material-icons">arrow_drop_down</i>`;
      else sortByDate.innerHTML = `<i class="material-icons">date_range</i><i class="material-icons">arrow_drop_up</i>`;
    }
    sortSlider.appendChild(sortByWord);
    sortSlider.appendChild(sortByExplaination);
    sortSlider.appendChild(sortByDate);
    template.appendChild(sortSlider);
    // eof sortSlider
    // rightMenu
    let rightMenu = generateElement('div', {class: "rightSlider", id: "wordsMenu", innerHTML: '<i class="material-icons">chevron_left</i>'});
    let rightMenuInside = generateElement('div');
    let openIndexCardsBtn = generateElement('button', {innerHTML: '<i class="material-icons">dynamic_feed</i>'});
    openIndexCardsBtn.onclick = function() {
      let form;
      this.style.backgroundColor = '#004488';
      if(group.wordsDisplayForm === 'cards') {
        form = 'list';
        this.style.backgroundColor = '#111';
      }
      else form = 'cards';
      group.refreshWords(form);
    }
    rightMenuInside.appendChild(openIndexCardsBtn);
    let downloadBtn = generateElement('button', {innerHTML: '<i class="material-icons">cloud_download</i>'});
    downloadBtn.onclick = () => group.downloadWordsTxt();
    rightMenuInside.appendChild(downloadBtn);
    rightMenu.appendChild(rightMenuInside);
    template.appendChild(rightMenu);
    // eof rightMenu
    if(group.wordsDisplayForm === 'cards') template.appendChild(GroupDisplayer.wordsToCardsNode(group.wordsToDisplay))
    else template.appendChild(GroupDisplayer.wordsToListNode(group.wordsToDisplay, group))
    Scene.displayNode(template);
  }
  static displayGroup = (group) => {
    let oldGroup = document.getElementById('groupDisplay');
    if(oldGroup) oldGroup.remove();
    let groupDisplay = document.createDocumentFragment();
    let groupDisplayContainer = generateElement('div', {id: 'groupDisplay'});
    let addUserForm = document.createElement('form');
    addUserForm.onsubmit = (e) => {
      e.preventDefault();
      let name = document.getElementById('addUserName').value;
      group.addUser(name);
    }
    addUserForm.appendChild(generateElement('input', {type: 'text', id: 'addUserName', placeholder: 'Username'}));
    addUserForm.appendChild(generateElement('button', {type: 'submit', innerHTML: 'Add user'}));
    let groupDisplayHeader = generateElement('h4', {innerHTML: `Group: ${group.name}`});
    groupDisplayContainer.appendChild(groupDisplayHeader);
    if(group.admins.map(x=>x._id).indexOf(app.user.id)!==-1) groupDisplayContainer.appendChild(addUserForm);
    let leaveGroupBtn = generateElement('button', {class: 'red', innerHTML: 'Leave group'});
    leaveGroupBtn.onclick = () => group.removeUser(app.user.id);
    groupDisplayContainer.appendChild(leaveGroupBtn);
    let usersList = document.createElement('ul');
    group.users.forEach(u => {
      let x = generateElement('li', {innerHTML: u.username})
      usersList.appendChild(x);
    })
    groupDisplayContainer.appendChild(usersList);
    groupDisplay.appendChild(groupDisplayContainer);
    document.querySelector('main').appendChild(groupDisplay);
    window.scrollTo({ top: findYOffset(document.getElementById("groupDisplay")), behavior: 'smooth' });
  }
  static wordsToCardsNode = (words) => {
    let template = document.createDocumentFragment();
    let wordsList = generateElement('div', {id: "wordsList"})
    words.forEach((w, i) => {
      let card = generateElement('div', {class: 'flip-card', id: `card${i}`});
      card.style.height = `${window.innerHeight*0.45}px`
      if(window.innerWidth<750) {
        card.style.marginTop = `${window.innerHeight*0.25}px`;
        card.style.marginBottom = `${window.innerHeight*0.35}px`;
      }
      card.style.width = `${window.innerWidth*0.85}px`
      if(w.flipped) card.classList.add('flipped')
      card.innerHTML =
      `<div class="flip-card-inner">
        <div class="flip-card-front">
          <p>${w.word}</p>
        </div>
        <div class="flip-card-back">
          <p>${w.explaination}</p>
        </div>
      </div>`;
      card.onclick = () => {
        if(w.flipped) card.classList.remove('flipped');
        else card.classList.add('flipped');
        w.flip();
      }
      wordsList.appendChild(card);
    })
    template.appendChild(wordsList);
    return template;
  }
  static wordsToListNode = (words, group) => {
    let template = document.createDocumentFragment();
    let wordForm = generateElement('form');
    let html = `
    <div class="row">
      <input type="text" id="newWord" class="six columns" placeholder="New word"/>
      <input type="text" id="newExplaination" class="six columns" placeholder="Explaination"/>
    </div>
    <div class="row">
      <select id="newWordTopic" class="six columns">
      <option disabled selected value="">Topic (optional)</option>`;
    group.units.forEach(u=>{
      let topics = u.topics;
      topics.forEach(t => {
        html+=`<option value="${t.id}">${t.name}</option>`
      })
    })
    html+=`</select>
    </div>
    <button type="submit">Add word</button>`;
    wordForm.innerHTML = html;
    wordForm.onsubmit = (e) => {
      e.preventDefault();
      let object = {
        word: document.querySelector('#newWord').value,
        explaination: document.querySelector('#newExplaination').value,
        group: group.id
      }
      let topic = document.querySelector('#newWordTopic').value;
      if(topic) object.topic = topic;
      let w = new Word(object);
      w.save();
    }
    let wordsList = generateElement('div', {id: "wordsList"});
    wordsList.appendChild(wordForm);
    words.forEach((w, i) => {
      let x = w.toNode();
      if(i%2===0) x.style.backgroundColor = '#e3e3e3';
      else x.style.backgroundColor = '#ffffff';
      wordsList.appendChild(x);
    })
    template.appendChild(wordsList);
    return template;
  }
  static updateWords(node) {
    document.querySelector('body').style.overflow = 'scroll';
    console.log(document.querySelector("#wordsList"))
    document.querySelector("#wordsList").innerHTML = '';
    document.querySelector("#wordsList").appendChild(node);
  }
}
