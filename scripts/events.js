class Events {
  #list;
  constructor() {
    this.#list = {};
  }
  register = (name, callback, index) => {
    if(!this.#list[name] || index===-1) this.#list[name] = [];
    if(typeof index === 'number' && index>=0) this.#list[name].splice(index, 0, callback)
    else this.#list[name].push(callback);
  }
  emit = (name, data) => {
    for (let i = 0; i < this.#list[name].length; i++) this.#list[name][i](data);
  }
  clear = (name) => {
    this.#list[name] = undefined; // is this correct?
  }
}
