"use strict";
class Main {
	#scene;
	#home;
	#groups;
	#settings;
	#currentGroup;
	constructor() {
		this.token = localStorage.getItem('token');
		this.reissue_token = localStorage.getItem('reissueToken');
		if(!this.token) window.location.href = '/login';
		if(!this.reissue_token) window.location.href = '/login';
		this.user = new User({username: localStorage.getItem('username'), id: localStorage.getItem('_id')});
		this.#groups = [];
		this.#home = new HomeScreen(this.user.username);
		this.#settings = new Settings(this.user.username);
    document.querySelectorAll('.loggedOutNav').forEach(x=>x.remove());
	}
	run = async() => {
		// if ('serviceWorker' in navigator) {
		// 	window.addEventListener('load', () => {
		// 		navigator.serviceWorker.register('sw')
		// 		.then(function(registration) {
		// 			console.log('ServiceWorker registration successful with scope: ', registration.scope);
		// 		},
		// 		(err) => {
		// 			console.log('ServiceWorker registration failed: ', err);
		// 		});
		// 	});
		// }
		// window.isUpdateAvailable = new Promise(async (resolve, reject) => {
		// 	if (!'serviceWorker' in navigator) reject(false);
		// 	let reg = await navigator.serviceWorker.register('sw');
		//   if(!reg) reject(false);
		//   if (!("Notification" in window)) alert("This browser does not support push notifications");
		//   else if (Notification.permission !== "denied") Notification.requestPermission();
		// 	reg.onupdatefound = () => {
		// 		const installingWorker = reg.installing;
		// 		installingWorker.onstatechange = () => {
		// 			switch (installingWorker.state) {
		// 				case 'installed':
		// 					if (navigator.serviceWorker.controller) return resolve(true);
		// 					return resolve(false);
		// 			}
		// 		};
		// 	};
		// });
		//
		// window['isUpdateAvailable']
		// .then(isAvailable => {
		//   if (isAvailable) toast("The app has just been updated! Reload it to start using new version!", 4000);
		// })
		// .catch(() => console.log("no update"))

		let defferedPrompt;
		let btnAdd = document.querySelector('#btnAdd');
		window.addEventListener('beforeinstallprompt', e => {
		  e.preventDefault();
		  defferedPrompt = e;
		  btnAdd.style.display = "block";
		})
		btnAdd.addEventListener('click', e => {
		  defferedPrompt.prompt();
		  defferedPrompt.userChoice
		  .then(res => {
		    if(res.outcome === "accepted") console.log('acc');
		    defferedPrompt = null;
		  })
		})
		events.register('newGroup', (group) => {
			this.#groups.push(group);
			this.openGroups();
		})
		events.register('newWord', () => this.#currentGroup.refreshWords())
		events.register('newTopic', () => this.openBook())
		events.register('newUnit', () => this.openBook())
		events.register('wordRemoved', () => this.#currentGroup.refreshWords())
		events.register('groupSelected', (group) => {
			this.#currentGroup = group;
			if(this.state === 'book') this.openBook();
			else if(this.state === 'dictionary') this.openDictionary();
		})
		events.register('leftGroup', (groupId) => {
			this.#groups = this.#groups.filter(x => x.id !== groupId);
			if(this.#currentGroup.id === groupId) this.#currentGroup = this.#groups[0]; // todo: handle noGroups exception
			this.openGroups();
		})
		await this.fetchGroups();
		if(this.#groups.length>0) this.#currentGroup = this.#groups[0];
		Scene.updateGroupsSelect(this.#groups);
		this.openHash();
		// remove SiteLoader here?
		document.querySelector('.loader').remove()

		// document.getElementById('logout').addEventListener('click', () => logOut());
		document.getElementById('book').addEventListener('click', () => {
			this.openBook();
		})
		document.getElementById('groups').addEventListener('click', () => {
			this.openGroups();
		})
		document.getElementById('home').addEventListener('click', () => {
			this.openHome();
		})
		document.getElementById('dictionary').addEventListener('click', () => {
			this.openDictionary();
		})
		document.getElementById('settings').addEventListener('click', () => {
			this.openSettings();
		})
	}
	fetchGroups = async() => {
		await fetch("youlang/user/groups/", {
			method: "GET",
			headers: setHeaders(),
			referrer: "no-referrer"
		})
		.then(async(res) => {
			if(!res.ok) {
				if(res.status === 403) {
					toast("Refreshing...", 1000);
					await reissueToken();
					await this.fetchGroups();
				}
			}
			else {
				console.log('fetched groups');
				let groups = await res.json();
				this.#groups = groups.map(g => new Group(g));
			}
		});
	}
	openBook = () => {
		this.state = 'book';
		Scene.updateState('book');
		if(!this.#currentGroup) return toast("Pick a grp", 4000);
		GroupDisplayer.templateBook(this.#currentGroup);
	}
	openDictionary = () => {
		this.state = 'dictionary';
		Scene.updateState('dictionary');
		GroupDisplayer.templateWords(this.#currentGroup);
	}
	openGroups = () => {
		this.state = 'groups';
		Scene.updateState('groups');
		let node = document.createDocumentFragment();
		let newGroupForm = generateElement('form', {class: 'row'});
		let newGroupName = generateElement('input', {type: 'text', id: 'newGroupName', placeholder: "'New groups's name", class: "six columns"});
		let newGroupDescription = generateElement('input', {type: 'text', id: 'newGroupDescription', placeholder: "'New groups's description", class: "six columns"});
		newGroupForm.appendChild(newGroupName);
		newGroupForm.appendChild(newGroupDescription);
		let newGroupSubmit = generateElement('input', {type: 'submit', value: 'Add group'});
		newGroupForm.appendChild(newGroupSubmit);
		newGroupForm.onsubmit = (e) => {
			e.preventDefault();
			let g = new newGroup({name: newGroupName.value, description: newGroupDescription.value});
			g.save();
		}
		node.appendChild(newGroupForm);
		node.appendChild(document.createElement('ul'));
		for(let i = 0; i<this.#groups.length; i++) {
			let x = document.createElement('li');
			x.innerHTML = this.#groups[i].name;
			x.onclick = () => GroupDisplayer.displayGroup(this.#groups[i]);
			node.childNodes[1].appendChild(x);
		}
		Scene.displayNode(node);
	}
	openHash = () => {
		if(window.location.hash === '#groups') {
			this.openGroups();
		}
		else if(window.location.hash === '#book') {
			this.openBook();
		}
		else if(window.location.hash === '#dictionary') {
			this.openDictionary();
		}
		else if(window.location.hash === '#settings') {
			this.openSettings();
		}
		else {
			this.openHome();
		}
	}
	openHome = () => {
		this.state = 'home';
		Scene.updateState('home');
		let node = this.#home.generateTemplate();
		Scene.displayNode(node);
	}
	openSettings = () => {
		this.state = 'settings';
		Scene.updateState('settings');
		let node = this.#settings.generateTemplate();
		Scene.displayNode(node);
		this.#settings.attachEvents();
	}
}

class User {
	#username;
	#id;
	constructor(object) {
		this.#username = object.username;
		this.#id = object.id;
	}
	get username() {
		return this.#username;
	}
	get id() {
		return this.#id;
	}
}

let events = new Events();
let app = new Main();
app.run();
